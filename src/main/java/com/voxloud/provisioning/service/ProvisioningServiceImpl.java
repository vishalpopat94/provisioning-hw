package com.voxloud.provisioning.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.voxloud.provisioning.entity.Device;
import com.voxloud.provisioning.repository.DeviceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.*;

@Service
public class ProvisioningServiceImpl implements ProvisioningService {

    @Autowired
    DeviceRepository deviceRepository;

    @Value("${provisioning.domain}")
    private String domain;

    @Value("${provisioning.port}")
    private String port;

    @Value("${provisioning.codecs}")
    private String codecs;

    final ObjectMapper mapper = new ObjectMapper();


    public String getProvisioningFile(String macAddress) throws Exception {
        // TODO Implement provisioning
        Optional<Device> deviceOptional = deviceRepository.findById(macAddress);
        if (deviceOptional.isEmpty()) {
            throw new Exception("Device not found");
        } else {

            Device device = deviceOptional.get();
            switch (device.getModel()) {
                case DESK:
                    Properties properties = new Properties();
                    properties.setProperty("username", device.getUsername());
                    properties.setProperty("password", device.getPassword());
                    properties.setProperty("domain", domain);
                    properties.setProperty("codecs", codecs.toString());
                    if (device.getOverrideFragment() != null) {
                        properties = overWriteProperties(properties, device.getOverrideFragment());

                    }
                    return propertiesToString(properties);
                case CONFERENCE:
                    Map<String, Object> configurationMap = new HashMap<>();
                    configurationMap.put("username", device.getUsername());
                    configurationMap.put("password", device.getPassword());
                    configurationMap.put("domain", domain);
                    configurationMap.put("codecs", codecs);
                    if (device.getOverrideFragment() != null) {
                        configurationMap = overWriteProperties(configurationMap, device.getOverrideFragment());

                    }
                    try {
                        return mapper.writeValueAsString(configurationMap);
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                default:
                    break;
            }
        }
        return null;
    }

    private String propertiesToString(Properties properties) {
        StringBuilder sb = new StringBuilder();
        Set<Object> objects = properties.keySet();
        for (Object object : objects) {
            sb.append("\n" + object + ":" + properties.get(object));
        }
        return sb.toString().trim();
    }

    private Properties overWriteProperties(Properties properties, String overrideFragment) throws IOException {
        properties.load(new StringReader(overrideFragment));
        return properties;
    }

    private Map<String, Object> overWriteProperties(Map<String, Object> properties, String overrideFragment) throws JsonProcessingException {
        Map<String, Object> objectMap = mapper.readValue(overrideFragment, new TypeReference<Map<String, Object>>() {
        });
        properties.putAll(objectMap);
        return properties;
    }
}
