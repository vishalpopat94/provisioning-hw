package com.voxloud.provisioning.controller;

import com.voxloud.provisioning.service.ProvisioningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class ProvisioningController {

    // TODO Implement controller method

    @Autowired
    ProvisioningService provisioningService;

    @GetMapping(path =  "provisioning/{macAddress}")
    public ResponseEntity<String> getProvisioningFile(@PathVariable String macAddress){
        String provisioningFile = null;
        try {
            provisioningFile = provisioningService.getProvisioningFile(macAddress);
        } catch (Exception e) {
            return  new ResponseEntity("Device not found" ,HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(provisioningFile, HttpStatus.OK);
    }

}